:: Source json folder
SET SOURCE="C:\Users\DucDM\Dropbox\Small Victories\tiamo\assets\json"

:: Destination json folder
SET DEST="D:\SourceTree\tiamo\src\assets\json"

:: Clear current destination folder
RMDIR /S /Q %DEST%

:: Copy source to dest
IF NOT EXIST %DEST% MKDIR %DEST%
XCOPY %SOURCE% %DEST% /S /E /H
