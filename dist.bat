:: Source distribution folder
SET SOURCE=D:\SourceTree\tiamo\dist\tiamo

:: Destination distribution folder
SET DEST=C:\Users\DucDM\Dropbox\Small Victories\tiamo

:: Clone .git to temp
SET TEMP_FOLDER=C:\Users\DucDM\Dropbox\Small Victories\temp
RMDIR  /S /Q "%TEMP_FOLDER%"
MKDIR "%TEMP_FOLDER%"
XCOPY "%DEST%\.git" "%TEMP_FOLDER%" /E /I

:: Clear current destination folder
RMDIR /S /Q %DEST%

:: Copy source to dest
IF NOT EXIST "%DEST%" MKDIR "%DEST%"
XCOPY "%SOURCE%" "%DEST%" /S /E /H /Y

:: Re-copy .git
XCOPY "%TEMP_FOLDER%\.git" "%DEST%" /E /I
RMDIR  /S /Q "%TEMP_FOLDER%"
