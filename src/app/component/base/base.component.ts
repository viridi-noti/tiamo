import { Component, OnInit } from '@angular/core';
import { ContentDeliveryService } from '../../service/content-delivery.service';
import { TestData } from '../../entity/testData';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss']
})
export class BaseComponent implements OnInit {
  data: TestData;

  constructor(private cds: ContentDeliveryService) { }

  ngOnInit() {
    this.cds.loadContent('assets/json/test.json').subscribe((newData: TestData) => {
      this.data = newData;
      console.log(this.data);
    });
  }

}
