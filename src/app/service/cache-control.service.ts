import { Cache } from '../entity/cache/Cache';
import { Cacheable } from '../entity/cache/cacheable';
import { CachedData } from '../entity/cache/cached-data';

const CACHE_PREFIX = 'SmartCache--';

export class CacheControlService {

  /** data storing object */
  private static cache: Cache = {};

  /**
   * register an object/value with cache
   * (if overwriting, has to clear cache first to eliminate child paths as well)
   *
   * @param path name of the unparsed key of which value is to be stored in cache
   * @param value cached value
   * @param overwrite overwrite stored value if existed, default = true
   * @returns true if successful, false if unsuccessful
   */
  public registerCache(path: string, value: Cacheable, overwrite: boolean = true): boolean {
    const key: string = CACHE_PREFIX + path;
    const current: Cache = CacheControlService.cache;

    // 1. case non-existing cache
    if (current[key] === null || current[key] === undefined) {
      CacheControlService.cache[key] = new CachedData(value);
      return true;
    }

    // 2. case existing cache
    // 2.1. case overwrite is true
    if (overwrite === true) {
      CacheControlService.cache[key] = new CachedData(value);
      this.clearCache(path);
      return true;
    }

    // 2.2 case overwrite is false
    return false;
  }

  /**
   * return the Cacheable contained by Cache if key is not null
   *
   * @param path name of the unparsed key of which value is stored in cache
   * @returns the cached Cacheable if exists, null if not
   */
  public getCache(path: string): Object {
    return null;
  }

  /**
   * @returns all existing cached paths in cache
   */
  public getAllCachedPaths(): string[] {
    return null;
  }

  /**
   * get the existing direct child paths of the specified path
   *
   * @param path specified path
   * @returns all direct child paths of the specified path
   */
  public getDirectChildPathsOf(path: string): string[] {
    return null;
  }

  /**
   * clear cache at the specified path, including child paths
   * (also delete the related attributes in Cache)
   *
   * @param path name of the unparsed key of which value stored in cache is to be cleared
   */
  public clearCache(path: String): void {
  }

  /**
   * clear all cached Cacheable
   * (also delete all caching attributes in Cache)
   */
  public clearAllCache(): void {

  }
}
