import { CacheControlService } from './cache-control.service';
import { TestBed } from '@angular/core/testing';

const initCacheControlService = (): CacheControlService => {
  TestBed.configureTestingModule({
    providers: [CacheControlService]
  });
  return TestBed.get(CacheControlService);
};

const CACHE = 'cache';
const CACHE_PREFIX = 'SmartCache--';

fdescribe('CacheControlService', () => {
  let service: CacheControlService;
  beforeEach(() => {
    service = initCacheControlService();
  });

  describe('CacheControlService - registerCache', () => {
    it('should successfully register a value to cache with "SmartCache--" added as prefix to the key', () => {
      const cachedData = { test: 'hello' };
      const key = 'data';
      const parsedKey = `${CACHE_PREFIX}${key}`;
      const result: boolean = service.registerCache(key, cachedData);
      expect(CacheControlService[CACHE][parsedKey]).toBeDefined();
      expect(result).toBe(true);
    });
  });
});
