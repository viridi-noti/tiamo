import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TestData } from '../entity/testData';

@Injectable()
export class ContentDeliveryService {
  constructor(private httpClient: HttpClient) {
  }

  public loadContent(assetPath: string): Observable<TestData> {
    return this.httpClient.get<TestData>(assetPath);
  }
}
