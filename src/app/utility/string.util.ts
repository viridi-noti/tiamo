import { StringConstant } from '../constant/string.const';

export class StringUtil {

  /**
   * return true if source is null or blank, else return false
   *
   * @param source source string
   */
  public static isNullOrEmpty(source: string) {
    if (source === null || source === undefined || source === StringConstant.BLANK) {
      return true;
    }

    return false;
  }

  /**
   * split source string by token
   *
   * @param source source string
   * @param token token to mark where to split
   */
  public static splitByToken(source: string, token: string): string[] {
    if (StringUtil.isNullOrEmpty(source)) {
      return [StringConstant.BLANK];
    }

    if (StringUtil.isNullOrEmpty(token)) {
      return [source];
    }
    return source.split(token);
  }

  /**
   * split source string by dot
   *
   * @param source source string
   */
  public static splitByDot(source: string): string[] {
    return StringUtil.splitByToken(source, StringConstant.DOT);
  }
}
