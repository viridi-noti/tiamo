import { StringUtil } from './string.util';

describe('StringUtil', () => {
  describe('StringUtil - isNullOrEmpty', () => {

    it('should return true with null', () => {
      const result: boolean = StringUtil.isNullOrEmpty(null);
      expect(result).toBe(true);
    });

    it('should return true with undefined', () => {
      const result: boolean = StringUtil.isNullOrEmpty(undefined);
      expect(result).toBe(true);
    });

    it('should return true with blank', () => {
      const result: boolean = StringUtil.isNullOrEmpty('');
      expect(result).toBe(true);
    });

    it('should return false with one letter', () => {
      const result: boolean = StringUtil.isNullOrEmpty('a');
      expect(result).toBe(false);
    });

    it('should return false with zero number string', () => {
      const result: boolean = StringUtil.isNullOrEmpty('0');
      expect(result).toBe(false);
    });
  });

  describe('StringUtil - splitByToken', () => {

    it('should return three strings on order', () => {
      const result: string[] = StringUtil.splitByToken('ab/bc/ca', '/');
      expect(result.length).toBe(3);
      expect(result[0]).toBe('ab');
      expect(result[1]).toBe('bc');
      expect(result[2]).toBe('ca');
    });

    it('should return two strings and 1 blank at the end', () => {
      const result: string[] = StringUtil.splitByToken('ab/bc/', '/');
      expect(result.length).toBe(3);
      expect(result[0]).toBe('ab');
      expect(result[1]).toBe('bc');
      expect(result[2]).toBe('');
    });

    it('should return two strings and 1 blank in the middle', () => {
      const result: string[] = StringUtil.splitByToken('ab//ca', '/');
      expect(result.length).toBe(3);
      expect(result[0]).toBe('ab');
      expect(result[1]).toBe('');
      expect(result[2]).toBe('ca');
    });

    it('return array with blank as sole element if source is null', () => {
      const result: string[] = StringUtil.splitByToken(null, '/');
      expect(result.length).toBe(1);
      expect(result[0]).toBe('');
    });

    it('return array with blank as sole element if source is blank', () => {
      const result: string[] = StringUtil.splitByToken(null, '/');
      expect(result.length).toBe(1);
      expect(result[0]).toBe('');
    });

    it('return array with source as sole element if token is null', () => {
      const source = 'hello';
      const result: string[] = StringUtil.splitByToken(source, null);
      expect(result.length).toBe(1);
      expect(result[0]).toBe(source);
    });

    it('return array with source as sole element if token is blank', () => {
      const source = 'hello';
      const result: string[] = StringUtil.splitByToken(source, '');
      expect(result.length).toBe(1);
      expect(result[0]).toBe(source);
    });

    it('return array with two blanks if source is token', () => {
      const source = 'hello';
      const result: string[] = StringUtil.splitByToken(source, source);
      expect(result.length).toBe(2);
      expect(result[0]).toBe('');
      expect(result[1]).toBe('');
    });
  });
});

