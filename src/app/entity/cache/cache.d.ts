import { CachedData } from './cached-data';
export interface Cache {
  [key: string]: CachedData;
}
