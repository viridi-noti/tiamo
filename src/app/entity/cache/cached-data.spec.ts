import { CachedData } from './cached-data';

fdescribe('CachedData', () => {
  describe('CachedData - constructor', () => {

    it('should initialize the data field with input data', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      expect(cachedData.getData()).toBe(data);
    });
  });

  describe('CachedData - setChild', () => {

    it('does not change the child\'s original value if null is input', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      const originalValue = ['abc'];
      cachedData.setChild(originalValue);
      cachedData.setChild(null);
      expect(cachedData.getChild()).toBe(originalValue);
    });

    it('does not change the child\'s original value if undefined is input', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      const originalValue = ['abc'];
      cachedData.setChild(originalValue);
      cachedData.setChild(undefined);
      expect(cachedData.getChild()).toBe(originalValue);
    });

    it('does change the child\'s original value if an empty array is input', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      const originalValue = ['abc'];
      const newValue = [];
      cachedData.setChild(originalValue);
      cachedData.setChild(newValue);
      expect(cachedData.getChild()).toBe(newValue);
    });
  });

  describe('CachedData - hasChild', () => {

    it('return true when one or more childs are set', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      cachedData.setChild(['abc']);
      expect(cachedData.hasChild).toBe(true);
    });

    it('return false when no childs are set', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      expect(cachedData.hasChild).toBe(false);
    });

    it('return false when an empty array is set', () => {
      const data = { test: 'hello' };
      const cachedData = new CachedData(data);
      cachedData.setChild(['abc']);
      cachedData.setChild([]);
      expect(cachedData.hasChild).toBe(false);
    });
  });
});

