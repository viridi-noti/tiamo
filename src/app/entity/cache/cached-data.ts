import { Cacheable } from './cacheable';

export class CachedData {
  private data: Cacheable;
  private childs: string[];
  public get hasChild(): boolean {
    return this.childs.length > 0;
  }

  constructor(data: Cacheable) {
    this.data = data;
    this.childs = [];
  }

  public setChild(childs: string[]) {
    if (childs === null || childs === undefined) {
      return;
    }
    this.childs = childs;
  }

  public getChild(): string[] {
    return this.childs;
  }

  public addChild(child: string) {
    this.childs.push(child);
  }

  public addData(data: Cacheable) {
    this.data = data;
  }

  public getData(): Cacheable {
    return this.data;
  }
}
