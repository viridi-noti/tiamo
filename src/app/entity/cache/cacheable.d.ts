export type Cacheable = string | number | boolean | Array<Object> | Object;
