import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BaseComponent } from './component/base/base.component';
import { BrowserModule } from '@angular/platform-browser';
import { CacheControlService } from './service/cache-control.service';
import { ContentDeliveryService } from './service/content-delivery.service';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';


@NgModule({
  declarations: [
    AppComponent,
    BaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    ContentDeliveryService,
    CacheControlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
